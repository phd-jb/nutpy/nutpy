tags_dict = {
    "TAT": "Total Access Time",
    "MAT": "Mean Access Time",
    "MAX": "Maximum Access Time",
    "PVS": r"$\rho$",
    "SGA": r"$G(\xi)$",
    "NOA": "Number of Accesses",
}

units_dict = {"TAT": "s", "MAT": "s", "MAX": "s", "PVS": None, "SGA": None, "NOA": None}

plot_options = {"cbar_size": 0.034, "projection": "mollweide"}

annual_motion = 2 * 3.1415 / (365 * 86400)
