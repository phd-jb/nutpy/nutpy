# API

Nutpy main class is called Nut. This class contains the main
methods to study the visibility between a rotating space telescope
and an ancillary satellite nearby.

```{eval-rst}
.. autoclass:: nutpy.mission.Nut
   :members:
```