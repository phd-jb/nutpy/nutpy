# Welcome to nutpy's documentation!

## About

nutpy is an open source Python library with a set of tools to study how
space-telescopes with spin-precession scan strategies observe the sky.


## Contents

```{toctree}
---
maxdepth: 2
caption: Getting started
---
installation
examples
```

```{toctree}
---
maxdepth: 2
caption: Reference
---
api
bib
```