# Bibliography


## Basic

* BERMEJO-BALLESTEROS, Juan, et al. Visibility Study in a Chief-Deputy Formation
for CMB Polarization Missions. The Journal of the Astronautical Sciences, 2022,
p. 1-41.
